CREATE USER ccarrier_user WITH encrypted password 'CCARRIER_YSER_PSQL_PASS';

/* Grant privileges for existing tables */
GRANT SELECT, INSERT, UPDATE, DELETE
ON ALL TABLES IN SCHEMA public
TO ccarrier_user;

/* Create related role with limited rights */
CREATE ROLE ccarrier_role WITH LOGIN;
GRANT ccarrier_role to zeus;
GRANT ccarrier_role to ccarrier_user;

/* Grant privileges for all future table create */
ALTER DEFAULT PRIVILEGES
	  FOR ROLE ccarrier_role
	  IN SCHEMA public
	  GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO ccarrier_user;

/* Grant privilegies on sequence created by Alembic */
GRANT UPDATE ON SEQUENCE yt_headers_id_seq TO ccarrier_user;
