#!/bin/bash
# Create secret containing HCI project's sepecific Sentry URI

set -e
source $OUTPUT_FUNC

export SECRET_NAME='content-carrier-metadata-stuffer'
MANIFEST_FILE="${MANIFEST_PATH}/metadata_stuffer.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	/bin/sed -i 's|NAPSTER_API_KEY_B64|'"$NAPSTER_API_KEY_B64"'|g' $MANIFEST_FILE
	unset NAPSTER_API_KEY_B64
	/bin/sed -i 's|SPOTIFY_CLIENT_ID_B64|'"$SPOTIFY_CLIENT_ID_B64"'|g' $MANIFEST_FILE
	unset SPOTIFY_CLIENT_ID_B64
	/bin/sed -i 's|SPOTIFY_CLIENT_SECRET_B64|'"$SPOTIFY_CLIENT_SECRET_B64"'|g' $MANIFEST_FILE
	unset SPOTIFY_CLIENT_SECRET_B64
	/bin/sed -i 's|ACOUSTID_API_KEY_B64|'"$ACOUSTID_API_KEY_B64"'|g' $MANIFEST_FILE
	unset ACOUSTID_API_KEY_B64
 	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE
fi
