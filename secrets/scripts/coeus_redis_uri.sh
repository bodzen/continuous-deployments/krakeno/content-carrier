#!/bin/bash
# Create secret containing HCI project's sepecific Coeus (=redis) uri

set -e
source $OUTPUT_FUNC

export SECRET_NAME='coeus-redis-credentials'
MANIFEST_FILE="${MANIFEST_PATH}/coeus_redis_uri.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	/bin/sed -i 's|COEUS_BROKER_CREDS_B64|'"$COEUS_BROKER_CREDS_B64"'|g' $MANIFEST_FILE
	unset SENTRY_URI_B64
	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE
fi
