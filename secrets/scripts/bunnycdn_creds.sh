#!/bin/bash
# Create secret containing BunnyCDN API Key

set -e
source $OUTPUT_FUNC

export SECRET_NAME='bunnycdn-credentials'
MANIFEST_FILE="${MANIFEST_PATH}/bunnycdn_creds.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	/bin/sed -i 's|BUNNY_CDN_CREDS_B64|'"$BUNNY_CDN_CREDS_B64"'|g' $MANIFEST_FILE
	unset BUNNY_CDN_CREDS_B64
	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE
fi
