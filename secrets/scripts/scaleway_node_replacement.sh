#!/bin/bash
# Create secret containing HCI project's sepecific Sentry URI

set -e
source $OUTPUT_FUNC

export SECRET_NAME='content-carrier-node-replacement'
MANIFEST_FILE="${MANIFEST_PATH}/scaleway_node_replacement.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	/bin/sed -i 's|NODE_RPLCMENT_API_KEY_B64|'"$NODE_RPLCMENT_API_KEY_B64"'|g' $MANIFEST_FILE
	unset NODE_RPLCMENT_API_KEY_B64
	/bin/sed -i 's|DATACENTER_REGION_B64|'"$DATACENTER_REGION_B64"'|g' $MANIFEST_FILE
	unset DATACENTER_REGION_B64
	/bin/sed -i 's|K8S_CLUSTER_ID_B64|'"$K8S_CLUSTER_ID_B64"'|g' $MANIFEST_FILE
	unset K8S_CLUSTER_ID_B64
	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE
fi
